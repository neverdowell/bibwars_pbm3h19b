﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    private const string FOLDER_RESOURCES_LOGO = "Logo"; // 400x226
    private const string FOLDER_RESOURCES_SPACESHIP_SPRITES = "Spaceships"; // 160 x 200
    private const string FOLDER_RESOURCES_LEVEL_SPRITES = "LevelSprites";
    private const string FOLDER_RESOURCES_LEVEL_PREFABS = "LevelPrefabs";

    [Header("Level Settings")]
    public int selectedLevelId = 0;
    public float levelScrollSpeed = 2;
    public float levelBackgroundScrollSpeed = 0.5f;


    // logo
    [Header("UI")]
    public Sprite logo;

    // spaceship
    [Header("Spaceships")]
    public Sprite[] spaceShipSprites;
    public int selectedSpaceShipId = 0;

    // level
    [Header("Levels")]
    public Sprite[] levelSprites;
    public GameObject[] levelPrefabs;
    




    #region singleton
    private static GameManager instance;
    private GameManager() { }

    public static GameManager GetInstance() {
        return instance;
    }

    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            instance.loadGameLogo();
            instance.loadSpaceShipSprites();
            instance.loadLevelSprites();
            instance.loadLevelPrefabs();
            DontDestroyOnLoad(this.gameObject);
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion




    #region helpers
    private void loadGameLogo() {
        Sprite[] loadedSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_LOGO);
        if (loadedSprites.Length > 0)
        {
            logo = loadedSprites[0];
        }
    }


    // spaceship helpers
    private void loadSpaceShipSprites() {
        spaceShipSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_SPACESHIP_SPRITES);
    }

    public Sprite GetSelectedSpaceShipSprite() {
        return spaceShipSprites[selectedSpaceShipId];
    }


    // level helpers
    private void loadLevelSprites() {
        levelSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_LEVEL_SPRITES);
    }

    private void loadLevelPrefabs() {
        levelPrefabs = Resources.LoadAll<GameObject>(FOLDER_RESOURCES_LEVEL_PREFABS);
    }

    public Sprite GetSelectedLevelSprite() {
        return levelSprites[selectedLevelId];
    }

    public GameObject GetSelectedLevelPrefab()
    {
        return levelPrefabs[selectedLevelId];
    }
    #endregion
}
