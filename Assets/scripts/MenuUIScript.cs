﻿using UnityEngine;
using UnityEngine.UI;

public class MenuUIScript : MonoBehaviour {

    public Image logoImage;

    #region helper
    private void Start() {
        GameManager gameManager = GameManager.GetInstance();
        if(gameManager.logo != null) {
            logoImage.sprite = gameManager.logo;
        }
    }
    #endregion
}
