﻿using UnityEngine;
using UnityEngine.UI;

public class SpaceShipSelectionScript : MonoBehaviour{

    public GameObject content;
    public ScrollRect scrollRect;
    public Scrollbar scrollbar;
    public GameObject spaceShipSelectionItemPrefab;
    public Color selectedColor;
    public Color unselectedColor;

    private GameManager gameManager;
    private GameObject[] spaceShipSelectionItems;




    #region lifecycle
    void Start(){
        gameManager = GameManager.GetInstance();
        Debug.Log("Instantiate SpaceShipSelectionItems");
        spaceShipSelectionItems = new GameObject[gameManager.spaceShipSprites.Length];
        for (int i = 0; i < gameManager.spaceShipSprites.Length; i++) {
            Sprite sprite = gameManager.spaceShipSprites[i];
            Debug.Log("Create selection item: " + sprite.name);
            spaceShipSelectionItems[i] = createSpaceShipSelectionItem(i, sprite.name, sprite);
        }
        setSelected(gameManager.selectedSpaceShipId);
        scrollbar.value = 1;
        scrollRect.verticalNormalizedPosition = 1; // set scroll position to top
    }
    #endregion




    #region helper
    public GameObject createSpaceShipSelectionItem(int id, string spaceShipName, Sprite spaceShipSprite) {
        GameObject spaceShipSelectionItem = Instantiate(spaceShipSelectionItemPrefab, content.transform);
        spaceShipSelectionItem.name = spaceShipName;
        SpaceShipSelectionItemScript spaceShipSelectionItemScript = spaceShipSelectionItem.GetComponent<SpaceShipSelectionItemScript>();
        spaceShipSelectionItemScript.spaceShipSelectionScript = this;
        spaceShipSelectionItemScript.id = id;
        spaceShipSelectionItemScript.spaceshipImage.sprite = spaceShipSprite;
        spaceShipSelectionItemScript.nameText.text = spaceShipName;

        return spaceShipSelectionItem;
    }

    public void setSelected(int selectedIndex) {
        gameManager.selectedSpaceShipId = selectedIndex;
        for (int i = 0; i < spaceShipSelectionItems.Length; i++) {
            UnityEngine.UI.Image image = spaceShipSelectionItems[i].GetComponent<SpaceShipSelectionItemScript>().backgroundImage;
            if (i == selectedIndex) {
                image.color = selectedColor;
            } else {
                image.color = unselectedColor;
            }
        }
    }
    #endregion
}
