﻿using UnityEngine;
using UnityEngine.UI;

public class TwinkleStarScript : MonoBehaviour {

	public float startAfterSeconds;
	public float twinkleDuration;
	public float pauseDuration;

	private Image image;




	/* lifecycle methods */
	void Start () 
	{
		// get image reference
		image = GetComponent<Image> ();

		// set image transparent
		image.CrossFadeAlpha (0.0f, 0.0f, true);

		// invoke fade in 
		Invoke ("fadeIn", startAfterSeconds);
	}




	/* helpers */
	// fades image in and invokes fade out after half twinkle duration
	public void fadeIn()
	{
		image.CrossFadeAlpha (1.0f, twinkleDuration / 2, false);
		Invoke ("fadeOut", twinkleDuration / 2);
	}

	// fades image out and invokes fade in after pause duration
	private void fadeOut()
	{
		image.CrossFadeAlpha (0.0f, twinkleDuration / 2, false);
		Invoke ("fadeIn", pauseDuration);
	}
}
