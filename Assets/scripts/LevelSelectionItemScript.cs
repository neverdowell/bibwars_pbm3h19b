﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionItemScript : MonoBehaviour{

    [Header("Settings")]
    public int id;
    public LevelSelectionScript levelSelectionScript;

    [Header("UI")]
    public Image backgroundImage;
    public Image levelImage;
    public Text nameText;
    public Text levelNumberText;




    #region helper
    public void SetSelected() {
        levelSelectionScript.setSelected(id);
    }
    #endregion
}
