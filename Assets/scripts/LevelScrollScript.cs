using UnityEngine;

public class LevelScrollScript : MonoBehaviour
{
    

    private float scrollSpeed; // Scroll speed of levelprefab
    private float backgroundScrollSpeed;




    #region lifecycle
    void Awake()
    {
        scrollSpeed = GameManager.GetInstance().levelScrollSpeed;
    }

    void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * scrollSpeed);
    }
    #endregion
}
