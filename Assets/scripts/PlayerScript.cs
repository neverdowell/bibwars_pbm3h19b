﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    [Header("Player")]
    public SpriteRenderer playerSpriteRenderer;
    public bool isAlive = true;
	public float speed = 3;
	public int points = 0;
	public Text text;
	public float minXPosition;
	public float maxXPosition;

    [Header("Points")]
	public GameObject goal;
	public GameObject score;
	public Text scoreText;

    [Header("Audio")]
	public AudioClip explosionSound;
	public AudioClip pickupSound;
	public AudioClip backgroundMusic;
	public AudioClip cheeringSound;

    [Header("Explosion")]
	public GameObject explosion;
	Animator animator;


    /* lifecycle methods */
    void Start (){
		animator = explosion.GetComponent<Animator>();
        playerSpriteRenderer.sprite = GameManager.GetInstance().GetSelectedSpaceShipSprite();
	}

	void Update () {
		// Get horizontal Axis value for Movement and calculate new position
		transform.position += Vector3.right * speed * Time.deltaTime * Input.GetAxis("Horizontal");
		if (transform.position.x < minXPosition) {  // check for left bound
			transform.position = new Vector3 (minXPosition, transform.position.y, 0);
		} else if (transform.position.x > maxXPosition){  // check for right bound
			transform.position = new Vector3 (maxXPosition, transform.position.y, 0);
		}
		// player is rotated denpending on horizontal axis speed (max 30 degrees)
		transform.rotation = Quaternion.Euler (0,0,Input.GetAxis("Horizontal") * -30);
	}
		
	void OnTriggerEnter2D(Collider2D collider){
		if (isAlive) {
			// If player collides with money, destroy money and add one point
			if (collider.tag.Equals ("coin")) {
				points++;
				text.text = points + " Points";
				AudioSource.PlayClipAtPoint (pickupSound, Vector3.zero);
				Destroy (collider.gameObject);
			}  // otherwise, if player hits an enemy, destroy player und invoke chanteToMenu
		else if (collider.tag.Equals ("enemy")) {
				isAlive = false;
				this.gameObject.GetComponent<Renderer> ().enabled = false;
				Destroy (collider.gameObject);
				AudioSource.PlayClipAtPoint (explosionSound, Vector3.zero);
				animator.SetTrigger ("startExplosion");
				speed = 0;
				showScoreHUD ();
			} else if (collider.tag.Equals ("ziel")) {
				showScoreHUD ();
				AudioSource.PlayClipAtPoint (cheeringSound, Vector3.zero);
			}
		}
	}





	/* helpers */
	public void ChangeToMenu(){
		checkAndSetHighscore ();
		SceneManager.LoadScene ("MenuScene");
	}

	// get current highscore and check weather current score is higher and set it 
	void checkAndSetHighscore(){
		int currentHighscore = PlayerPrefs.GetInt ("highscore");
		if (currentHighscore < points) {
			PlayerPrefs.SetInt ("highscore", points);
		}
	}

	// show scoreHUD and set scoreText
	void showScoreHUD(){
		score.SetActive (true);
		scoreText.GetComponent<Text> ().text = points.ToString ();
		Invoke ("ChangeToMenu", 4f); // 1.3
	}
}
