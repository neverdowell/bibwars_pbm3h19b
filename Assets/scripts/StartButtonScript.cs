﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartButtonScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Text startButtonText;
    public Color textColorDefault = new Color(0, 143, 254);
    public Color textColorHovering = new Color(255, 204, 1);

    #region lifecycle
    private void OnMouseEnter()
    {
        Debug.Log("OnMouseEnter");
        startButtonText.color = textColorHovering;
    }

    private void OnMouseExit()
    {
        startButtonText.color = textColorDefault;
    }
    #endregion

    #region helper
    public void buttonClick(string sceneName)
	{
		SceneManager.LoadScene (sceneName);
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        startButtonText.color = textColorHovering;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        startButtonText.color = textColorDefault;
    }
    #endregion
}
